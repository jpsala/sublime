import sublime, sublime_plugin

class RevealInSideBarAndFocusCommand(sublime_plugin.WindowCommand):
    def run(self):
        self.window.run_command("reveal_in_side_bar")
        self.window.run_command("focus_side_bar")